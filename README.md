# README

1 - (For readers who know CSS) Create a new micropost, then use your browser’s HTML inspector to determine the CSS id for the text “Micropost was successfully created.” What happens when you refresh your browser?
A mensagem é exibida somente em uma requisição. Quando a página é atualizada ela não aparece mais. 


2 - Try to create a micropost with empty content and no user id.
Um novo micropost será criado com content e user vazio pos não existe validação no model.

1 - Try to create a micropost with the same long content used in a previous exercise (Section 2.3.1.1). How has the behavior changed?

Sim, pois existe uma validação para que o usuário não deixe o micropost em branco nem maior que 140 caracteres. O usuário também é preciso ser válido (cadastrado no banco de dados). Nos exercícios anteriores a validação não existia. 

2 - (For readers who know CSS) Use your browser’s HTML inspector to determine the CSS id of the error message produced by the previous exercise.
error_explanation


